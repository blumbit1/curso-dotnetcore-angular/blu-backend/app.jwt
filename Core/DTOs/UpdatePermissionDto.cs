﻿using System.ComponentModel.DataAnnotations;

namespace App.JWT.Demo.Core.DTOs
{
    public class UpdatePermissionDto
    {
        [Required(ErrorMessage = "UserName is required")]
        public string? UserName { get; set; }       
    }
}
