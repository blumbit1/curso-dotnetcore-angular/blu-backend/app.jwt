﻿using System.ComponentModel.DataAnnotations;

namespace App.JWT.Demo.Core.DTOs
{
    public class LoginDto
    {
        [Required(ErrorMessage = "UserName is required")]
        public string? UserName { get; set; } 

        [Required(ErrorMessage = "Password is required")]
        public string? Password { get; set; }
    }
}
