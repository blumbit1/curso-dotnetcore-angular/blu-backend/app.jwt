﻿namespace App.JWT.Demo.Core.DTOs
{
    public class AuthServiceResponseDto
    {
        public bool IsSucceed { get; set; }
        public string? Message { get; set; }
    }
}
